#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "huffman.h"

node add(tree *t, node *n1, node *n2) {						//Attachs nodes to tree
	node q, *p = (node*)malloc(sizeof(node)), *m1 = (node*)malloc(sizeof(node)), *m2 = (node*)malloc(sizeof(node));
	*m1 = *n1;
	*m2 = *n2;
	p->c = '\0';
	p->left = m1;
	p->right = m2;
	p->prob = m1->prob + m2->prob;
	*t = p;			
	q = *p;
	free(p);
	return q;
}

void create_tree(tree *t, node *n, int count) {	//Creates tree from given array of ndoes
	int i, j, min;
	node temp;

	while(count != 1) {
		for(i = 0; i < count - 1; i++) {
			min = i; 
			for(j = i + 1; j < count; j++)
				if(n[j].prob < n[min].prob)
					min = j;
			if(i != min) {
				temp = n[i];
				n[i] = n[min];
				n[min] = temp;
			}
		}
		n[0] = add(t, &n[0], &n[1]);
		n[1] = n[--count];
	}
	free(n);
}

void writetree(FILE *fp2, node *n, int ncount) {		//Write nodes data to file fp2
	int i;
	fwrite(&ncount, sizeof(int), 1, fp2);				//Writes number of nodes contained
	for(i = 0; i < ncount; i++) {
		fwrite(&n[i].c, sizeof(char), 1, fp2);
		fwrite(&n[i].prob, sizeof(int), 1, fp2);
	}
	return;
}

void createtreefromfile(tree *t, FILE *fp, FILE *fp2) {	//Creates tree using file fp
	int j = 0, k = 0, flag;		//flag to indicate if char node is already present
	char c;
	node *n = NULL;
	while((c = fgetc(fp))) {
		if(c == EOF)
			break;
		k = flag = 0;
		while(k < j) {				//to find if node with char already exists
			if(n[k].c == c) {		//if exists, increments occurance variable
				n[k].prob++;
				flag = 1;
				break;
			}
			k++;
		}
		if(!flag) {					//if node does not already exist, creates one
			n = realloc(n, (sizeof(node) * j) + sizeof(node));
			n[j].c = c;
			n[j].prob = 0;
			n[j].path = NULL;
			n[j].left = n[j].right = NULL;
			j++;
		}
	}
	writetree(fp2, n, j);
	create_tree(t, n, j);
	fseek(fp, 0, SEEK_SET);
	return;
}

void readtree(tree *t, FILE *fp2) {		//Reads nodes data written using writetree()
	int ncount, i = 0;		//ncount is total count of nodes
	node *n = NULL;
	fread(&ncount, sizeof(int), 1, fp2);	//reads number of nodes to create
	n = (node*)malloc(sizeof(node) * ncount);
	while(i < ncount) {
		fread(&n[i].c, sizeof(char), 1, fp2);
		fread(&n[i].prob, sizeof(float), 1, fp2);
		i++;
	}
	create_tree(t, n, ncount);
	return;
}
void destroytree(tree t){				//frees all malloc memory
	if(t == NULL)
		return;
	destroytree(t->left);
	destroytree(t->right);
	free(t);
}