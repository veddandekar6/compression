typedef struct node {
	char c;
	int prob;
	char *path;
	struct node *left, *right;	
} node;

typedef node* tree;

node add(tree *t, node *n1, node *n2);
void create_tree(tree *t, node *n, int count);
void writetree(FILE *fp2, node *n, int ncount);
void readtree(tree *t, FILE *fp2);
void createtreefromfile(tree *t, FILE *fp, FILE *fp2);
void destroytree(tree t);