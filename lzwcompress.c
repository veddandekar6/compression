#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "lzw.h"
void BinaryConvert(int n, char *bin) { 			//Converts decimal n to binary in bin
	int i = 11; 
	while(n > 0) {
		bin[i] = '0' + (n % 2); 
		n /= 2;
		i--; 
	} 
	while(i >= 0)
		bin[i--] = '0';
	return;
} 

void writetofile(FILE *fp2, int code) {			//Write data to file
	static int k = 0;
	static char w;
	int i;
	char *bin = (char*) malloc(sizeof(char) * 13); //to store binary
	bin[12] = '\0';
	if(code != -999) {			//Handles general call
		if(code < 0)		//To handle extended ASCII
			code += 256;
		BinaryConvert(code, bin);
		for(i = 0; i < 12; i++) {				//Encode bits into char w
			if(bin[i] == '1') {
				w = w | (0x01);
				if(k != 7)
					w = w << 1;
				k++;
			}
			if(bin[i] == '0') {
				w = w & (0xFE);
				if(k != 7)
					w = w << 1;
				k++;
			}
			if(k == 8) {
				fwrite(&w, sizeof(char), 1, fp2);
				k = 0;
			}
		}
	}
	else if(k != 0) {						//Fills remaining bits with 0
		w = w << (7 - k);
		fwrite(&w, sizeof(char), 1, fp2);
		k = 0;
	}
	free(bin);
	return;
}

int LZWcompress(char *fname, char *fname2) {		//return error number if error occurs, else returns 0
	int code, code2, read;	//code2 keeps a copy of code when required, read keeps track of no. of chars read
	int count, flag = 0;	//flag to indicate end of file
	FILE *fp = NULL, *fp2 = NULL;
	char *ch = NULL;
	list l;
	fp = fopen(fname, "r");
	if(!fp) {
        perror("File could not be opened");
        return errno;
    }
	fp2 = fopen(fname2, "w");
	if(!fp2) {
        perror("File could not be opened");
        return errno;
    }
	init(&l);
	ch = (char*)malloc(sizeof(char) * 3);
	while((read = fread(ch, sizeof(char), 2, fp))) {
		ch[read] = '\0';
		count = read;
		code2 = flag = 0;
		while((code = searchlist(&l, ch, ch[count - 1]))) {		//Checks if string already present in dictionary
			if(feof(fp)){
				flag = 1;
				break;
			}
			fseek(fp, -(count * sizeof(char)), SEEK_CUR);		
			ch = (char*)realloc(ch, sizeof(char) * (count + 2));
			ch[strlen(ch) + 1] = '\0';
			fread(ch, count + 1, 1, fp);
			count++;
			code2 = code;
		}
		if(code2 || flag){
			writetofile(fp2, code2);
		}
		else{
			writetofile(fp2, (int) ch[0]);
		}
		if(feof(fp))
			break;
		append(&l, ch, ch[count - 1]);
		fseek(fp, -1, SEEK_CUR);
	}
	writetofile(fp2, -999);			//flushes any data left to write to file
	fclose(fp);
	fclose(fp2);
	free(ch);
	destroylist(&l);
	printf("\nCompressed successfully\n");
	printf("File has been saved as %s\n", fname);
	return 0;
}
