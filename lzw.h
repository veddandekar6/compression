typedef struct node{
	int code;
	char c, *prefix;
	struct node *next, *prev;
} node;

typedef struct list{
	node *head, *tail;
} list;

void init(list *l);
void append(list *l, char* prefix, char c);
int searchlist(list *l, char* prefix, char c);
char* searchcode(list *l, int code);
void destroylist(list *l);
void printlist(list *l);