#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
int huffcompress(char *fname, char *fname2);
int huffdecompress(char *fname, char *fname2);
int LZWcompress(char *fname, char *fname2);
int LZWdecompress(char *fname, char *fname2);
int main(int argc, char* argv[]) {
	int ch;
	if(argc != 4){
		printf("Incorrect Arguments\nCorrect Usage: ./project code inputfile outputfile\nRefer to README.md for available codes\n");
		return 0;
	}
	
	if(strcmp(argv[1], "-c1") == 0)
		ch = 1;
	else if(strcmp(argv[1], "-c2") == 0)
		ch = 3;
	else if(strcmp(argv[1], "-uc1") == 0)
		ch = 2;
	else if(strcmp(argv[1], "-uc2") == 0)
		ch = 4;
	else
		ch = 99;

	switch(ch) {
		case 0: break;
		case 1: errno = huffcompress(argv[2], argv[3]);
			break;
		case 2: errno = huffdecompress(argv[2], argv[3]);
			break;
		case 3: errno = LZWcompress(argv[2], argv[3]);
			break;
		case 4: errno = LZWdecompress(argv[2], argv[3]);
			break;
		default: printf("Invalid Input\n");
	}
	return errno;
}
