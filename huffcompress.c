#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "huffman.h"

char *retpath = NULL;
int reti, globflag;								//reti keeps count of no of bytes to malloc
												//globflag = 0 when entering
												//globflag = 1 when searching
												//globflag = 2 when path found

void setglobalpath(tree t, char c) {			//To set global variable retpath to point
	static char* path;							//to the path of char c
	if(!globflag && !reti) {
		path = (char*)malloc(sizeof(char));
		retpath = (char*)malloc(sizeof(char));
	}
	globflag = 1;
	if(t->c == c) {
		retpath = (char*)realloc(retpath, reti * sizeof(char) + sizeof(char));
		strncpy(retpath, path, reti);
		retpath[reti] = '\0';
		free(path);
		globflag = 2;
		return;
	}
	if(t->left && globflag != 2){
		path = (char*)realloc(path, reti * sizeof(char) + sizeof(char));
		reti++;
		path[reti - 1] = '0';
		setglobalpath(t->left, c);
	}
	if(t->right && globflag != 2) {
		path = (char*)realloc(path, reti * sizeof(char) + sizeof(char));
		reti++;
		path[reti - 1] = '1';
		setglobalpath(t->right, c);
	}
	reti--;
}

int huffcompress(char *fname, char *fname2) {	//return error number if error occurs, else returns 0
	int j = 0, k = 0, ignore = 0;	//j, k loop variables. Ignore to count bits to skip
	tree t;
	char c, w, ch[2];				//c to read, w to write, ch for ignore bits.
	FILE *fp = NULL, *fp2 = NULL;
	fp = fopen(fname, "r");
	if(!fp) {
        perror("File could not be opened");
        return errno;
    }
	fp2 = fopen(fname2, "w");
	if(!fp2) {
        perror("File could not be opened");
        return errno;
    }
	createtreefromfile(&t, fp, fp2);
	while((c = fgetc(fp))) {
		if(c == EOF)
			break;
		reti = globflag = 0;
		setglobalpath(t, c);
		for(j = 0; j < strlen(retpath); j++) {			//codes bits into char w an writes to file
			if(k == 8) {
				fwrite(&w, sizeof(char), 1, fp2);
				k = 0;
			}
			if(retpath[j] == '1') {
				w = w | (0x01);
				if(k != 7)
					w = w << 1;
				k++;
			}
			if(retpath[j] == '0') {
				w = w & (0xFE);
				if(k != 7)
					w = w << 1;
				k++;
			}
		}
		free(retpath);
	}
	while(k != 8) {				//Fills remaining bits in char with 0
		w = w | (0x01);
		if(k != 7)
			w = w << 1;
		k++;
		ignore++;				//counts unnecessary bits
	}	
	fwrite(&w, sizeof(char), 1, fp2);
	sprintf(ch, "%d", ignore);				//converts to char to save space
	fwrite(ch, sizeof(char), 1, fp2);		//writes count of unnecessary bits as last char
	fclose(fp);
	fclose(fp2);
	destroytree(t);
	printf("\nCompressed successfully\n");
	printf("File has been saved as %s\n", fname);
	return 0;
}