#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "huffman.h"
void decode_data(FILE *fp, FILE *fp2, tree t) {
	int i = 0, k, ignore = 0, pcount = 0, pos;	//i, k for loop, ignore for ignore count, pcount to count number of chars, pos to store position of fp2
	char c, end;		//c to read, end to store ignore bits count.
	tree torig = t;
	pos = ftell(fp2);
	while(fread(&c, sizeof(char), 1, fp2))
		pcount++;
	fseek(fp2, pos, SEEK_SET);
	while(i < pcount - 2) {					//Traverses tree to find corresponding char
		fread(&c, sizeof(char), 1, fp2);
		for(k = 0; k != 8; k++) {
			if(c & (0x80>>k))
				t = t->right;
			else
				t = t->left;
			if(t->c != '\0'){
				fwrite(&t->c, sizeof(char), 1, fp);		//when found, writes char to file
				t = torig;
			}
		}
		i++;
	}
	fread(&c, sizeof(char), 1, fp2);
	fread(&end, sizeof(char), 1, fp2);
	ignore = atoi(&end);
	for(k = 0; k != (8 - ignore); k++) {				//for reading specific no. of bits from last char
		if(c & (0x80>>k))
			t = t->right;
		else
			t = t->left;

		if(t->c != '\0'){
			fwrite(&t->c, sizeof(char), 1, fp);
			t = torig;
		}
	}
	fclose(fp);
}

int huffdecompress(char *fname, char *fname2) {			//return error number if error occurs, else returns 0
	tree t;
	FILE *fp = NULL, *fp2 = NULL;
	fp2 = fopen(fname, "r");
	if(!fp2) {
        perror("File could not be opened");
        return errno;
    }
	fp = fopen(fname2, "w");
	if(!fp) {
        perror("File could not be opened");
        return errno;
    }
	readtree(&t, fp2);									//Reads tree data from file
	decode_data(fp, fp2, t);
	fclose(fp2);
	destroytree(t);
	printf("\nDecompressed succesfully\n");
	printf("File has been saved as %s\n", fname);
	return 0;
}