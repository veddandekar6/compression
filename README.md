# Compression

Project by 
Name: Ved Dandekar		MIS: 111708072

Usage/Syntax: ./project code inputfile outputfile

	Available codes are:
							-c1 for Huffman Compression
							-c2 for LZW Compression
							-uc1 for Huffman Decompression
							-uc2 for LZW Decompression

Project contains code to implement the two different compression algorithms - Huffman Coding and LZW to compress as well as to decompress a given data file. The Huffman Coding algorithm has been implemented using a binary tree which can be traversed to find the path(code) to each character present in the file. The algorithm is based on the fact that some character appear much more often in a file than the rest and can hence be represented by data of smaller size. LZW, on the other hand, is based on reoccurance of specific patterns of data. These patterns are then represented by data of a smaller size. This project uses linked lists to implement LZW algorithm.

Steps for Huffman
	- Compression
		- Read file, create node for each distinct character present.
		- Add nodes to tree in order of increasing frequency of occurance.
		- Assign path to each leaf node as follows:
			- Every right traverse adds a '1' to path.
			- Every left traverse adds a '0' to path.
		- Write char and assigned paths to file.
		- Read chars from begining of file, output path of each char to file. Paths are coded as bits into char.
		- After all chars have been written, if excess bits are present, fill with 0 and write number of void bits as the next and last char.

	- Decompression
		- Read chars & assigned paths and add to tree.
		- Read comrpessed data bit by bit and traverse tree as follows:
			- For every 0, traverse left.
			- For ever 2, traverse right.
			- On encountering leaf node, print char stored in node and shift back to root.
		- on reaching (n - 1)th char, read nth char and read only that number of bits from (n - 1)th

Steps for LZW
	- Compression
		- Read char from file, attach to prefix(prefix initialized empty).
		- Check if prefix present in dictionary, if not add to dictionary. Assign code.
		- Write char code to file as binary of 12 bits.

	- Decompression
		- Read 12 bits from file.
		- Check if code present in dictionary:
			- If present, write string to file.
			- If not, add last added prefix + prefix[0]  to dicitonary and write to file.