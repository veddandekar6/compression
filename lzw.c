#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lzw.h"
#define MAX 4095

void init(list *l) {
	l->head = l->tail = NULL;
}
void append(list *l, char* prefix, char c) {
	int code;
	node *n = l->head, *p = NULL;
	if((n = l->tail)) {
		code = (l->tail->code) + 1;
		if(code > MAX)
			return;
	}
	p = (node*)malloc(sizeof(node));
	if(strlen(prefix) == 1){
		p->prefix = (char*)malloc(sizeof(char) * 2);
		strncpy(p->prefix, prefix, 1);
		p->prefix[1] = '\0';
	}
	else {
		p->prefix = (char*)malloc(sizeof(char) * strlen(prefix));
		strncpy(p->prefix, prefix, strlen(prefix));
		p->prefix[strlen(prefix) - 1] = '\0';
	}
	p->c = c;
	p->next = p->prev = NULL;
	if(!n) {
		l->head = l->tail = p;
		p->code = 256;
		return;
	}
	n = l->tail;
	n->next = p;
	p->prev = n;
	p->code = code;
	if(n == l->tail)
		l->tail = p;
	return;
}
int searchlist(list *l, char* prefix, char c) {		//Searches list to check if already present
	node *n = l->head;								//returns 0 if not present
	char *p = (char*)malloc(sizeof(char) * strlen(prefix));
	strncpy(p, prefix, strlen(prefix));
	p[strlen(prefix) - 1] = '\0';
	if(!n){
		free(p);
		return 0;
	}
	while(n) {
		if((n->c == c) && (strcmp(n->prefix, p) == 0)) {
			free(p);
			return n->code;
		}
		n = n->next;
	}
	free(p);
	return 0;
}
char* searchcode(list *l, int code) {		//Searches and returns code from dictionary
	node *n = NULL;							//returns NULL if not present
	char *ch = NULL;
	if(code < (l->tail->code / 2)) {
		n = l->head;
		while(n) {
			if(n->code == code) {
				ch = (char*)malloc(sizeof(char) * (strlen(n->prefix) + 2));
				ch[strlen(n->prefix) + 1] = '\0';
				strcpy(ch, n->prefix);
				ch[strlen(n->prefix)] = n->c;
				return ch;
			}
			n = n->next;
		}
	}
	else {
		n = l->tail;
		while(n) {
			if(n->code == code) {
				ch = (char*)malloc(sizeof(char) * (strlen(n->prefix) + 2));
				ch[strlen(n->prefix) + 1] = '\0';
				strcpy(ch, n->prefix);
				ch[strlen(n->prefix)] = n->c;
				return ch;
			}
			n = n->prev;
		}
	}
	return NULL;
}

void destroylist(list *l) {						//Frees all malloc memory
	if(!l->head)
		return;
	node *n = l->tail->prev;
	while(n) {
		free(n->next);
		n = n->prev;
	}
	free(l->head);
	l->head = l->tail = NULL;
}

void printlist(list *l) {
	node *n = l->head;
	while(n) {
		printf("%d\t", n->code);
		printf("%s %c\n", n->prefix, n->c);
		n = n->next;
	}
}