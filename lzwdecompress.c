#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "lzw.h"
int toint(char *str) {					//Convert binary string to decimal
	int n = 0, i;
    for(i = 0; i < 12; ++i) {
        n <<= 1;
        n += str[i] - '0';
    }
    return n;
}

int retcode(FILE *fp) {				//Reads comrpessed data and returns decimal code
	static char ch;
	static int k = 0;
	char str[12];
	int i = 0, j, end;
	if(!k)
		end = fread(&ch, sizeof(char), 1, fp);
	if(!end)
			return 0;
	for(j = 0; j != 12; j++) {
		if(k == 8) {
			end = fread(&ch, sizeof(char), 1, fp);
			k = 0;
			if(!end)
				return 0;
		}
		if(ch & (0x80 >> k++))
			str[i++] = '1';
		else
			str[i++] = '0';
	}
	return toint(str);
}

void formlist(FILE *fp, FILE *fp2, list *l){
	int code, i;
	char *w = (char*)malloc(sizeof(char)), *prefix = NULL, *passer = (char*)malloc(sizeof(char)); //prefix stores prefix, w stores postfix, passer stores part of entire string to pass
	prefix = (char*)malloc(sizeof(char) * 3);
	prefix[2] = '\0';
	while((code = retcode(fp))){
		//printf("CODE: %d", code);
		if(code < 256) {					//If code belongs to standard ASCII
			w = (char*)realloc(w, sizeof(char) * 2);
			w[0] = (char)code;
			w[1] = '\0';
			fwrite(w, sizeof(char), 1, fp2);
			prefix = (char*)realloc(prefix, sizeof(char) * (strlen(prefix) + 2));
			prefix[strlen(prefix) + 1] = '\0';
			strcat(prefix, w);
		}
		else{
			free(w);
			w = NULL;
			if(!(w = searchcode(l, code))){		//If code not present in dictionary yet
				prefix = (char*)realloc(prefix, sizeof(char) * (strlen(prefix) + 2));
				prefix[strlen(prefix) + 1] = '\0';
				prefix[strlen(prefix)] = 'a';
				append(l, prefix, prefix[0]);
				prefix[0] = '\0';
			}

			w = searchcode(l, code);
			fwrite(w, sizeof(char) * strlen(w), 1, fp2);
			prefix = (char*)realloc(prefix, sizeof(char) * (strlen(prefix) + strlen(w) + 1));
			prefix[strlen(prefix) + strlen(w)] = '\0';
			strcat(prefix, w);
		}
		i = 2;
		passer = (char*)realloc(passer, sizeof(char) * (strlen(prefix) + 1));
		while(i <= strlen(prefix)) {	//To find smallest string not present in dictionary
			strncpy(passer, prefix, i);
			passer[i] = '\0';
			if(!searchlist(l, passer, passer[i - 1])) {
				append(l, passer, passer[i - 1]);
				break;
			}
			i++;
		}
		prefix = (char*)realloc(prefix, sizeof(char) * (strlen(w) + 1));	//Updates prefix for next loop
		strcpy(prefix, w);
		prefix[strlen(w)] = '\0';
	}
	free(w);
}

int LZWdecompress(char *fname, char *fname2) {			//return error number if error occurs, else returns 0
	FILE *fp = NULL, *fp2 = NULL;
	list l;
	init(&l);
	fp = fopen(fname, "r");
	if(!fp) {
        perror("File could not be opened");
        return errno;
    }
	fp2 = fopen(fname2, "w");
	if(!fp2) {
        perror("File could not be opened");
        return errno;
    }
	formlist(fp, fp2, &l);
	fclose(fp);
	fclose(fp2);
	destroylist(&l);
	printf("\nDecompressed successfully\n");
	printf("File has been saved as %s\n", fname);
	return 0;
}